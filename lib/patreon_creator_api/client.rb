class PatreonCreatorApi::Client
  def initialize(creator_access_token:)
    @creator_access_token = creator_access_token
  end

  def get_campaigns(opts = {})
    base_url = "campaigns"
    url = build_url(
      base_url,
      opts[:includes],
      opts[:fields],
      opts[:count],
      opts[:cursor]
    )
    get_parse_json(url)
  end

  def get_posts(opts = {})
    base_url = "campaigns/#{opts[:campaign_id]}/posts"
    url = build_url(
      base_url,
      opts[:includes],
      opts[:fields],
      opts[:count],
      opts[:cursor]
    )
    get_parse_json(url)
  end

  def get_identity(opts = {})
    base_url = "identity"
    url = build_url(base_url, opts[:includes], opts[:fields])
    get_parse_json(url)
  end

 private

  def get_parse_json(suffix)
    json = get_json(suffix)
    response = parse_json(json)

    if response.errors&.any?
      exception_message = response.errors.map do |error|
        "#{error["code_name"]}: #{error["detail"]}"
      end

      raise exception_message.join(", ")
    end

    response
  end

  def get_json(suffix)
    http = Net::HTTP.new("www.patreon.com", 443)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Get.new("/api/oauth2/v2/#{suffix}")
    req['Authorization'] = "Bearer #{@creator_access_token}"
    req['User-Agent'] =
      "PatreonCreatorApi Ruby Gem #{PatreonCreatorApi::VERSION}"

    http.request(req).body
  end

  def parse_json(json)
    JSON::Api::Vanilla.parse(json)
  end

  def build_url(url, includes=nil, fields=nil, count=nil, cursor=nil)
    parsed_url = URI.parse(url)
    params = parsed_url.query ? Rack::Utils.parse_query(parsed_url.query) : {}
    params['include'] = joined_or_null(includes) if includes
    fields.each do |name, val|
      params["fields[#{name}]"] = val.respond_to?(:join) ? val.join(',') : val
    end if fields
    params["page[count]"] = count if count
    params["page[cursor]"] = cursor if cursor
    query = params.empty? ? "" : "?#{Rack::Utils.build_query(params)}"

    "#{parsed_url.path}#{query}"
  end

  def joined_or_null(list_or_string)
    list_or_string && list_or_string.empty? ? "null" : Array(list_or_string).join(',')
  end
end
