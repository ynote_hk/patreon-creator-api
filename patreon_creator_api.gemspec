
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "patreon_creator_api/version"

Gem::Specification.new do |spec|
  spec.name          = "patreon_creator_api"
  spec.version       = PatreonCreatorApi::VERSION
  spec.authors       = ["Fanny Cheung @ynote_hk"]
  spec.email         = ["dev@ynote.hk"]

  spec.summary       =
    "Ruby gem to access Patreon API with Creator's access token"
  spec.description   =
    "This Ruby gem lets you access the Patreon API with your Creator's access" \
    "token. Please refer to Patreon's API documentation for more information."
  spec.homepage      = "https://gitlab.com/ynote_hk/patreon-creator-api"
  spec.license       = "MIT"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(spec)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'json-api-vanilla', '~> 1.0.1'
  spec.add_dependency 'rack', '~> 2.0.5'

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry", "~> 0.10.4"
end
