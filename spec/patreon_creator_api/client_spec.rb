RSpec.describe PatreonCreatorApi::Client do
  let(:creator_access_token) { "alice-in-wonderland" }
  let(:http) { instance_double(Net::HTTP) }
  let(:response) { instance_double(Net::HTTPResponse) }
  let(:body) { { data: [], errors: [] }.to_json }
  let(:request) { instance_double(Net::HTTP::Get) }
  let(:client) { PatreonCreatorApi::Client.new(creator_access_token: creator_access_token) }

  before do
    allow(Net::HTTP).to receive(:new).and_return(http)
    allow(http).to receive(:use_ssl=)
    allow(http).to receive(:verify_mode=)
    allow(http).to receive(:request).and_return(response)
    allow(response).to receive(:body).and_return(body)
    allow(Net::HTTP::Get).to receive(:new).and_return(request)
    allow(request).to receive(:[]=)
  end

  describe "#get_campaigns" do
    context "with default options" do
      before { client.get_campaigns }

      it "calls the Patreon API with the parameters for campaigns object" do
        expect(Net::HTTP).to have_received(:new).once.with("www.patreon.com", 443)
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns")
        expect(request).to have_received(:[]=).once.with("Authorization", "Bearer #{creator_access_token}")
        expect(request).to have_received(:[]=).once.with("User-Agent", "PatreonCreatorApi Ruby Gem #{PatreonCreatorApi::VERSION}")
        expect(http).to have_received(:request).once.with(request)
      end
    end

    context "when response returns errors" do
      let(:error) do
        {

          "code_name" => "Error",
          "detail" => "Something went wrong",
        }
      end

      before do
        allow(response).to receive(:body).and_return({ data: [], errors: [error] }.to_json)
      end

      it "raises an error" do
        expect { client.get_campaigns }.to raise_error(RuntimeError, "Error: Something went wrong")
      end
    end

    context "with includes" do
      before { client.get_campaigns(includes: "creator") }

      it "calls the Patreon API with the includes parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns?include=creator")
      end
    end

    context "with fields" do
      before { client.get_campaigns(fields: { campaign: "created_at" }) }

      it "calls the Patreon API with the fields parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns?fields%5Bcampaign%5D=created_at")
      end
    end

    context "with count" do
      before { client.get_campaigns(count: 10) }

      it "calls the Patreon API with the count parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns?page%5Bcount%5D=10")
      end
    end

    context "with cursor" do
      before { client.get_campaigns(cursor: "1234") }

      it "calls the Patreon API with the cursor parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns?page%5Bcursor%5D=1234")
      end
    end
  end

  describe "#get_posts" do
    context "with default options" do
      before { client.get_posts(campaign_id: "1234") }

      it "calls the Patreon API with the parameters for posts object" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns/1234/posts")
      end
    end

    context "with includes" do
      before { client.get_posts(campaign_id: "1234", includes: "creator") }

      it "calls the Patreon API with the includes parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns/1234/posts?include=creator")
      end
    end

    context "with fields" do
      before { client.get_posts(campaign_id: "1234", fields: { post: "created_at" }) }

      it "calls the Patreon API with the fields parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns/1234/posts?fields%5Bpost%5D=created_at")
      end
    end

    context "with count" do
      before { client.get_posts(campaign_id: "1234", count: 10) }

      it "calls the Patreon API with the count parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns/1234/posts?page%5Bcount%5D=10")
      end
    end

    context "with cursor" do
      before { client.get_posts(campaign_id: "1234", cursor: "1234") }

      it "calls the Patreon API with the cursor parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/campaigns/1234/posts?page%5Bcursor%5D=1234")
      end
    end
  end

  describe "#get_identity" do
    context "with default options" do
      before { client.get_identity }

      it "calls the Patreon API with the parameters for identity object" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/identity")
      end
    end

    context "with includes" do
      before { client.get_identity(includes: "creator") }

      it "calls the Patreon API with the includes parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/identity?include=creator")
      end
    end

    context "with fields" do
      before { client.get_identity(fields: { identity: "created_at" }) }

      it "calls the Patreon API with the fields parameter" do
        expect(Net::HTTP::Get).to have_received(:new).once.with("/api/oauth2/v2/identity?fields%5Bidentity%5D=created_at")
      end
    end
  end
end
