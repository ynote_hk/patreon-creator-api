[![pipeline status](https://gitlab.com/ynote_hk/patreon-creator-api/badges/main/pipeline.svg)](https://gitlab.com/ynote_hk/patreon-creator-api/-/commits/main) ![Version](https://img.shields.io/badge/version-v0.1.0-blue) [![License](https://img.shields.io/badge/license-hippocratic%20license-green)](LICENSE.md)

# Patreon Creator API

Patreon Creator API is a Ruby gem that lets you access the Patreon API v2 with a
[Creator's access token](https://www.patreon.com/portal/registration/register-clients).

The gem is under development, it is not yet published on a Gem registry.

## Motivation

As the [official Patreon gem](https://github.com/Patreon/patreon-ruby/pull/27)
doesn't handle the [Patreon API v2](https://docs.patreon.com/#apiv2-oauth) and
the maintenance on it
[seems to have stopped since May 2020](https://github.com/Patreon/patreon-ruby/pull/27),
I initiated this gem to be able to fetch
[my own posts on Patreon](https://www.patreon.com/ynote_hk/posts) with an
[Creator's access token](https://www.patreon.com/portal/registration/register-clients).
As a consequence, **this gem doesn't handle the OAuth login**. You can check the
[official Patreon gem](https://github.com/Patreon/patreon-ruby) if you need to
OAuth login.

## Pre-requisites

- Ruby >= 2.6.3
- Bundler >= 1.17.2

## Code example

```ruby
require "patreon_creator_api"

client = PatreonCreatorApi::Client.new(
  creator_access_token: ***,
)

posts_response = client.get_posts(
  campaign_id: ***,
  fields: {
    post: "title,published_at",
  },
)
posts = posts_response.data

posts.each do |post|
  puts "#{post.title} - Published at: #{post.published_at}"
end
```

## Installation

Add this line to your application's Gemfile:
```ruby
gem "patreon_creator_api"
```

And run:
```sh
$ bundle
```

Or install it yourself as:
```sh
$ gem install patreon_creator_api
```

## Development

Install the dependencies:
```sh
$ bin/setup
```

Run the tests:
```sh
$ bin/rspec
```

For an interactive prompt, you can run:
```sh
$ bin/console
```

## Usage

This section in in work in progress. It will be updated soon! 🐳

## Contributing

As the gem is under initial development, I don't accept Merge Request on this
project. A code contribution requires a lot of time to review, to comment and
to maintain. Even the smallest one can require hours of my time. I don't have
enough energy to handle that.

You are welcome to fork the repository and make the change you need 🍂

## Code of Conduct

Everyone interacting in the `patreon_creator_api` project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](CODE_OF_CONDUCT.md).

## Credits

Special thanks to:
- [@maximeCony](https://github.com/maximeCony) whose
  [code](https://github.com/Patreon/patreon-ruby/pull/27) structured the base of
  this gem,
- [@marienfressinaud](https://github.com/marienfressinaud) whose [great project
  lusio](https://github.com/flusio/flusio)'s README inspired me for this gem's
  README,
- [@sunny](https://github.com/sunny/) who inspired me every day.

## Resources

- [Patreon API documentation](https://docs.patreon.com/#introduction)
- [Official Patreon API Ruby gem](https://github.com/Patreon/patreon-ruby)

## Licence

The module is available as open source under the terms of [the Hippocratic
License](LICENSE.md).
